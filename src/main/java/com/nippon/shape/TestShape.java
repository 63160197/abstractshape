/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.shape;

/**
 *
 * @author Nippon
 */
public class TestShape {

    public static void main(String[] args) {
        Circle c1 = new Circle(1.5);
        Circle c2 = new Circle(4.5);
        Circle c3 = new Circle(5.5);
        System.out.println(c1);
        System.out.println(c2);
        System.out.println(c3);
        System.out.println("----------");
        Rectangle r1 = new Rectangle(3, 2);
        Rectangle r2 = new Rectangle(4, 3);
        System.out.println(r1);
        System.out.println(r2);
        System.out.println("----------");
        Square s1 = new Square(2.0);
        Square s2 = new Square(4.0);
        System.out.println(s1);
        System.out.println(s2);
        System.out.println("----------");

        Shape[] shapes = {c1, c2, c3};
        for (int i = 0; i < shapes.length; i++) {
            System.out.println(shapes[i].getName() + " area :" + shapes[i].calArea());
        }
        System.out.println("---------------Bye---------------");

        Shape[] shapes1 = {r1, r2};
        for (int i = 0; i < shapes1.length; i++) {
            System.out.println(shapes1[i].getName() + " area :" + shapes1[i].calArea());
        }
        System.out.println("---------------Bye---------------");

        Shape[] shapes2 = {s1, s2};
        for (int i = 0; i < shapes2.length; i++) {
            System.out.println(shapes2[i].getName() + " area :" + shapes2[i].calArea());
        }
        System.out.println("---------------Bye---------------");
        System.out.println("**********END PROJECT**********");
    }
    
}
